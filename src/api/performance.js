import { get, post } from '@/api/request'
// ############### 账户管理
// 获取账户列表
export const getAccountList = (data) => post('/api/admin/account/search', data)

// 获取账户冻结
export const accountDisable = (data) => post('/api/admin/account/disable', data, {
  headers: {
    'content-type': 'application/x-www-form-urlencoded'
  }
})

// 获取账户解冻
export const accountEnable = (data) => post('/api/admin/account/enable', data, {
  headers: {
    'content-type': 'application/x-www-form-urlencoded'
  }
})

// 平台分润流水列表
export const getSettleBillPlatform = (data) => post('/api/admin/settle/bill/platform/search', data)

// 师傅分润流水列表
export const getSettleBillWorker = (data) => post('/api/admin/settle/bill/worker/search', data)

