import { get, post } from '@/api/request'

// 获取订单列表
export const getOrderList = (data) => post('/api/admin/order/search', data)

// 获取订单详情
export const getOrderDetail = (data) => get(`/api/admin/order/get`, data)

// 获取订单日志
export const getOrderLog = (data) => get(`/api/admin/order/log/get`, data)

// 获取未抢列表
export const getDemandOrderList = (data) => post('/api/admin/demand/order/search', data)

