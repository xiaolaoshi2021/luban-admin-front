import { get, post } from '@/api/request'

// 待审核供应商列表
export const getAccessProvider = (data) => post('/api/admin/demand/provider/search', data)

// 创建供应商
export const accessSave = (data) => post('/api/admin/demand/provider/save', data)

// 获取供应商详情
export const getProviderDetail = (data) => get('/api/admin/demand/provider/audit/info', data)

// 审核通过供应商列表
export const getAccessAuditedProvider = (data) => post('/api/admin/demand/provider/audited/search', data)

// 启用
export const providerEnable = (data) => post('/api/admin/demand/provider/enable', data, {
  headers: {
    'content-type': 'application/x-www-form-urlencoded'
  }
})

// 禁用
export const providerDisable = (data) => post('/api/admin/demand/provider/disable', data, {
  headers: {
    'content-type': 'application/x-www-form-urlencoded'
  }
})

// 调整分润比例
export const providerModifyScale = (data) => post('/api/admin/demand/provider/modify/scale', data)

// 审核
export const providerAuditPrepare = (data) => post('/api/admin/demand/provider/audit', data)

// 需求单列表
export const getDemandOrder = (data) => post('/api/admin/demand/order/search', data)

