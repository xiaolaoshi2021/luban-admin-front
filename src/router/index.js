import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

/* Layout */
import Layout from '@/layout'
export const constantRoutes = [
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: '首页', icon: 'dashboard', affix: true, noCache: true }
      }
    ]
  },
  {
    path: '/worker',
    meta: { title: '师傅管理', icon: 'el-icon-s-goods' },
    name: 'Worker',
    redirect: '/worker',
    component: Layout,
    children: [
      {
        path: '/worker/examine',
        component: () => import('@/views/worker/examine.vue'),
        meta: { title: '师傅审核', icon: 'el-icon-s-flag', needAuth: true },
        name: 'workerExamine'
      },
      {
        path: '/worker/list',
        component: () => import('@/views/worker/list.vue'),
        meta: { title: '师傅列表', icon: 'el-icon-goods', needAuth: true },
        name: 'workerList'
      },
      {
        path: '/examine/status/detail',
        component: () => import('@/views/worker/examineDetail.vue'),
        meta: { title: '审核详情', icon: 'el-icon-s-flag', needAuth: true },
        name: 'ExamineStatusDetail',
        hidden: true
      }
    ]
  },
  {
    path: '/basic',
    meta: { title: '基础信息 ', icon: 'el-icon-s-promotion' },
    name: 'Basic',
    redirect: '/basic',
    component: Layout,
    children: [
      {
        path: '/service/category/list',
        component: () => import('@/views/basic/serviceCategory.vue'),
        meta: { title: '服务类别', icon: 'el-icon-s-ticket', needAuth: true },
        name: 'ServiceCategory'
      },
      {
        path: '/service/area/list',
        component: () => import('@/views/basic/serviceArea.vue'),
        meta: { title: '服务区域', icon: 'el-icon-s-comment', needAuth: true },
        name: 'ServiceArea'
      }
    ]
  },
  {
    path: '/order',
    meta: { title: '工单管理', icon: 'el-icon-s-order' },
    name: 'Order',
    redirect: '/order',
    component: Layout,
    children: [
      {
        path: '/order/list',
        component: () => import('@/views/order/orderList.vue'),
        meta: { title: '工单列表', icon: 'el-icon-menu', needAuth: true },
        name: 'orderList'
      },
      {
        path: '/order/detail',
        component: () => import('@/views/order/orderDetail.vue'),
        meta: { title: '工单详情', needAuth: true },
        name: 'orderDetail',
        hidden: true
      },
      {
        path: 'dispatch/order/list',
        component: () => import('@/views/order/dispatchOrderList.vue'),
        meta: { title: '工单调度列表', icon: 'el-icon-s-data', needAuth: true },
        name: 'DispatchOrderList'
      }
    ]
  },
  {
    path: '/supplier',
    meta: { title: '供应商管理', icon: 'el-icon-s-finance' },
    name: 'Supplier',
    redirect: '/supplier',
    component: Layout,
    children: [
      {
        path: '/supplier/examine/list',
        component: () => import('@/views/supplier/examineList.vue'),
        meta: { title: '供应商审核', icon: 'el-icon-s-cooperation', needAuth: true },
        name: 'SupplierExamineList'
      },
      {
        path: '/supplier/examine/detail',
        component: () => import('@/views/supplier/examineDetail.vue'),
        meta: { title: '供应商审核详情', needAuth: true },
        name: 'SupplierExamineDetail',
        hidden: true
      },
      {
        path: '/supplier/settled/list',
        component: () => import('@/views/supplier/settledInList.vue'),
        meta: { title: '已入驻供应商', icon: 'el-icon-s-shop', needAuth: true },
        name: 'SupplierSettledList'
      },
      {
        path: '/supplier/access/order/list',
        component: () => import('@/views/supplier/accessOrderList.vue'),
        meta: { title: '需求单列表', icon: 'el-icon-s-check', needAuth: true },
        name: 'AccessOrderList'
      }
    ]
  },
  {
    path: '/performance',
    meta: { title: '履约管理', icon: 'el-icon-s-custom' },
    name: 'Performance',
    redirect: '/performance',
    component: Layout,
    children: [
      {
        path: '/account/list',
        component: () => import('@/views/performance/accountList.vue'),
        meta: { title: '账户管理', icon: 'el-icon-s-opportunity', needAuth: true },
        name: 'AccountList'
      },
      {
        path: '/profit/shar',
        component: () => import('@/views/performance/profitShar.vue'),
        meta: { title: '平台分润', icon: 'el-icon-s-check', needAuth: true },
        name: 'ProfitShar'
      },
      {
        path: '/worker/profit/shar',
        component: () => import('@/views/performance/workerProfitShar.vue'),
        meta: { title: '师傅分润', icon: 'el-icon-share', needAuth: true },
        name: 'WorkerProfitShar'
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index.vue'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
