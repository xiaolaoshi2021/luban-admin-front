// 师傅状态
export const auditStatusData = [
  {
    value: 0,
    name: '驳回'
  },
  {
    value: 2,
    name: '未审核'
  }
]

// 审核状态
export const auditStatusMap = {
  0: '驳回',
  1: '通过',
  2: '未审核'
}

// 驳回原因
export const rejectReasonData = [
  {
    value: 1,
    name: '资质不全'
  },
  {
    value: 2,
    name: '身份证照片不清晰'
  },
  {
    value: 3,
    name: '其他'
  }
]
